<h1>OneAlbumADay - Album tracking app</h1>
<p> Simple app created with Python, Tkinter and PSQL.<br/>
Built for storing data about the albums I listen to.<\p>

<h2>Gui Screenshots</h2><br/>
<img src="https://i.imgur.com/PmUtAJn.gif" alt="Adding album to db trough the GUI"/><br/>
<img src="https://i.imgur.com/CGlOusD.png" alt="PSQL screenshot, confirming that the album was added to the db"/><br/>
