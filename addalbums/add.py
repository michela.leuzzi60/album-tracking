import psycopg2
import time
from config import Config
from dotenv import load_dotenv, find_dotenv
import os
import sys
import os.path
import sys
# sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# file_dir = os.path.dirname(__file__)
# sys.path.append(file_dir)


#psql password 
load_dotenv(find_dotenv())
PASSWORD=os.getenv('PSQL_PASSWORD')

def insert_album(album_name, artist, genre, date_listened):
    an = album_name.strip().lower()
    a = artist.strip().lower()
    g = genre.strip().lower()
    d = date_listened.strip().lower() 

    insert_to_db(an, a, g, d)

def insert_to_db(album_name, artist, genre, date_listened):
    conn = None
    try:
        if ((album_name == "") | (artist == "") | (genre == "") | (date_listened == "")):
            raise ValueError("Can't use empty values") 

        conn = psycopg2.connect(host="localhost", database="postgres", port="5432", user="postgres", password=PASSWORD)

        if conn is not None:
            print('Connection established to PostgreSQL.')
        else:
            print('Connection not established to PostgreSQL.')

        cur = conn.cursor()
        sql = "INSERT INTO album(name, artist, genre, date_listened) VALUES(%s, %s, %s, %s);"
        cur.execute(sql, [album_name, artist, genre, date_listened]) 
        conn.commit()
        cur.close()
        print("DONE!!!")
    except (Exception, psycopg2.DatabaseError) as e:
        print(e)
        raise
    finally:
        if conn is not None:
            conn.close()



