import tkinter as tk
from tkinter import *
from datetime import datetime
import tkcalendar
import traceback
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..')) #otherwise it doesn't recognize addalbums
from addalbums import add

class Application(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        self.add_widgets()
    
    def add_widgets(self):
        #labels
        self.intro = tk.Label(root, text="What did you listen to, lately?", font=('Raleway', 15)).pack(side="top", pady=17)
        self.album = tk.Label(root, text="Album name: ", font=('Raleway', 11)).place(x=30,y=50)
        self.artist = tk.Label(root, text="Artist(s): ", font=('Raleway', 11)).place(x=30,y=90)
        self.genre = tk.Label(root, text="Genre: ", font=('Raleway', 11)).place(x=30,y=130)
        self.date = tk.Label(root, text="When did you listen to it? (YYYY-MM-DD)", font=('Raleway', 11)).place(x=30,y=170)
        #maybe use tkcalendar instead

        #textboxes
        self.albuminput = tk.Entry(root)
        self.albuminput.place(x=30,y=70, width=400)
        self.artistinput = tk.Entry(root)
        self.artistinput.place(x=30,y=110, width=400)
        self.genreinput = tk.Entry(root)
        self.genreinput.place(x=30,y=150, width=400)
        self.calendar = tkcalendar.DateEntry(root, font=('Raleway', 11))
        self.calendar.place(x=30, y=195)

        #button
        button = tk.Button(root, text="Enter", font=('Raleway', 13), command= lambda: self.execute_db_input())
        button.place(relx=0.5, rely=0.80, anchor=CENTER)
        self.statuslabel = Label(text="", font=20)
        self.statuslabel.pack(side="bottom")
        
    def execute_db_input(self):
        albumin = self.albuminput.get()
        artistin = self.artistinput.get()
        genrein = self.genreinput.get()
        calendarinput = self.calendar.get_date()
        datein = calendarinput.strftime("%Y-%m-%d")
        try:
            add.insert_album(albumin, artistin, genrein, datein)
            self.statuslabel["text"] = "album successfully added to db"
        except:
            self.statuslabel["text"] = "album not added to db"
            traceback.print_exc()

        self.albuminput.delete(0, len(albumin)+1) #clears field when it's entered
        self.artistinput.delete(0, len(artistin)+1)
        self.genreinput.delete(0, len(genrein)+1)

    #for some reason stopped working 
    # def remove_label(self):
    #     self.statuslabel.after(1000, self.clear_label())

    # def clear_label(self):
    #     print("clear_label")
    #     self.statuslabel["text"] = " "


    def oof(self):
        print("oof")


root = tk.Tk()
root.title("OneAlbumADay :)")
root.geometry("500x300")
app = Application(master=root)
app.mainloop() #wrap everyting 