DROP TABLE IF EXISTS album;

CREATE TABLE album (
    name varchar(255),
    artist varchar(255),
    genre varchar(255) NOT NULL,
    date_listened date NOT NULL,
    CONSTRAINT pk_albums PRIMARY KEY (name, artist)
);

--INSERT INTO album VALUES ('XO', 'Elliot Smith', 'indie rock', '02-02-2021');

--select * from album;